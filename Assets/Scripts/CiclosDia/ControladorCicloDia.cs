using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class ControladorCicloDia : MonoBehaviour
{
    [SerializeField] private Light2D luzGlobal;
    [SerializeField] private CicloDia[] ciclosDia;
    [SerializeField] private float tiempoPorCiclo;

    private float TiempoActualCiclo = 0;
    private float porcentajeCiclo;
    private int CicloActual = 0;
    private int CicloSiguiente = 1;

    private void Start()
    {
        luzGlobal.color = ciclosDia[0].colorCiclo;
    }

    private void Update()
    {
        TiempoActualCiclo += Time.deltaTime;
        porcentajeCiclo = TiempoActualCiclo / tiempoPorCiclo;
        if (TiempoActualCiclo >= tiempoPorCiclo)
        {
            TiempoActualCiclo = 0;
            CicloActual = CicloSiguiente;

            if (CicloSiguiente + 1 > ciclosDia.Length -1)
            {
                CicloSiguiente = 0;
            }
            else
            {
                CicloSiguiente += 1;
            }
        }
        CambiarColor(ciclosDia[CicloActual].colorCiclo, ciclosDia[CicloSiguiente].colorCiclo);
    }

    private void CambiarColor(Color colorActual, Color siguienteColor)
    {
        luzGlobal.color = Color.Lerp(colorActual,siguienteColor,porcentajeCiclo);
    }

}
