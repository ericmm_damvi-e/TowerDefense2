using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class CicloDia
{
    public string nombreCiclo;
    public Color colorCiclo;
}
