        using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuInicial : MonoBehaviour
{
    public void play()
    {
        SceneManager.LoadScene("Juego");
    }

    public void goOptions()
    {
        SceneManager.LoadScene("Options");
    }
    public void goCredits()
    {
        SceneManager.LoadScene("Credits");
    }
    public void Exit()
    {
        UnityEditor.EditorApplication.isPlaying = false;
    }



}
